# Sudo and pkexec wrapper scripts for Linux-based systems

**These Bash scripts require the admin user to input the generated random number to grant root access.**

## Table of contents

- [Requirements](#requirements)
- [Install](#install)
- [Change configuration settings](#change-configuration-settings)
- [Usage](#usage)
- [Uninstall](#uninstall)

## Requirements

Make sure that the following packages are installed in the system:

*sudo  
policykit-1  
python3-gi or python3-pyqt5*

## Install

Place the *sudo* and *pkexec* under the */usr/local/bin* directory and make them executable.

```sh
sudo wget -O /usr/local/bin/sudo bit.ly/2ZQrNaA
sudo wget -O /usr/local/bin/pkexec bit.ly/2QGpUsQ
sudo chmod +x /usr/local/bin/{sudo,pkexec}
```

## Change configuration settings

```sh
sudo nano /usr/local/bin/sudo
sudo nano /usr/local/bin/pkexec
```

![](https://cdn.rios.hu/dl/upc/2020-01/12/180556_nkscxggclu1e7moi_nano_2.png)

Press *Ctrl+O* to save the changes, and *Ctrl+X* to exit from nano.

## Usage

These scripts replace the sudo and pkexec password dialogs or add an additional verification step.

To avoid to require the password input, set the following sudoer rules:

```sh
sudo sh -c "echo \"$USER ALL=NOPASSWD: /usr/local/bin/sudo *\" >/etc/sudoers.d/sudo"
sudo sh -c "echo \"$USER ALL=NOPASSWD: /usr/bin/bash -c /usr/local/bin/pkexec *\" >/etc/sudoers.d/pkexec"
sudo chmod 0440 /etc/sudoers.d/*
```

![](https://cdn.rios.hu/dl/upc/2020-01/12/180556_ub9yeoxqqpewk09o_pkexec-synaptic_3.png)

## Uninstall

Just remove the previously installed files.

```sh
sudo rm /usr/local/bin/{sudo,pkexec} /etc/sudoers.d/{sudo,pkexec}
```

